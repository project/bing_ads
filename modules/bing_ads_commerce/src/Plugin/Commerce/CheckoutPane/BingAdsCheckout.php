<?php

namespace Drupal\bing_ads_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the completion message pane.
 *
 * Hijack the pane form subsystem so that we can
 * call our addEvent for initialize checkout on the
 * first stage of checkout.
 *
 * @CommerceCheckoutPane(
 *   id = "BingAds_checkout",
 *   label = @Translation("BingAds Checkout"),
 *   default_step = "order_information",
 * )
 */
class BingAdsCheckout extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Only fire the FB event on page load.
    if (!$form_state->getTriggeringElement()) {
      /** @var \Drupal\bing_ads\BingAdsEvent $BingAds_event */
      $BingAds_event = \Drupal::service('bing_ads.BingAds_event');
      /** @var \Drupal\bing_ads_commerce\BingAdsCommerceInterface $BingAds_commerce */
      $BingAds_commerce = \Drupal::service('bing_ads_commerce.BingAds_commerce');
      $data = $BingAds_commerce->getOrderData($this->order);
      $BingAds_event->addEvent('InitiateCheckout', $data);
    }
    return [];
  }

}
