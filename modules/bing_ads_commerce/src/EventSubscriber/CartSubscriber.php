<?php

namespace Drupal\bing_ads_commerce\EventSubscriber;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\bing_ads\BingAdsEventInterface;
use Drupal\bing_ads_commerce\BingAdsCommerceInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CartSubscriber implements EventSubscriberInterface {

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The BingAds event service.
   *
   * @var \Drupal\bing_ads\BingAdsEventInterface
   */
  protected $BingAdsEvent;

  /**
   * The BingAds Commerce service.
   *
   * @var \Drupal\bing_ads_commerce\BingAdsCommerce
   */
  protected $BingAdsCommerce;

  /**
   * Constructs a new OrderEventSubscriber object.
   *
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\bing_ads\BingAdsEventInterface $BingAds_event
   *   The BingAds Event service.
   * @param \Drupal\bing_ads_commerce\BingAdsCommerceInterface $BingAds_commerce
   *   The BingAds Commerce service.
   */
  public function __construct(CartProviderInterface $cart_provider, BingAdsEventInterface $BingAds_event, BingAdsCommerceInterface $BingAds_commerce) {
    $this->cartProvider = $cart_provider;
    $this->BingAdsEvent = $BingAds_event;
    $this->BingAdsCommerce = $BingAds_commerce;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ENTITY_ADD => 'addToCart',
      'commerce_order.place.pre_transition' => 'finalizeCart',
    ];
  }

  /**
   * Add to cart event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The cart entity add event.
   */
  public function addToCart(CartEntityAddEvent $event) {
    $data = $this->BingAdsCommerce->getOrderItemData($event->getOrderItem());
    // Trigger the AddToCart event and force a session to be used.
    $this->BingAdsEvent->addEvent('add_to_cart', $data, TRUE);
  }

  /**
   * Finalize cart event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function finalizeCart(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $data = $this->BingAdsCommerce->getOrderData($order);
    $this->BingAdsEvent->addEvent('Purchase', $data);
  }

}
