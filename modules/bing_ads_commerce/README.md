# Bing Ads for Drupal Commerce


## Installation instructions.
* Enable this module
* Visit the configuration form for Bing Ads
on `/admin/config/bing_ads`
* If you have a custom checkout flow, make sure
BingAds Checkout is the on the first checkout
pane in beyond the "Login" page on
`/admin/commerce/config/checkout-flows`.
