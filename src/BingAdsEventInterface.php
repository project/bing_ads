<?php

namespace Drupal\bing_ads;

/**
 * Interface BingAdsEventInterface.
 *
 * @package Drupal\bing_ads
 */
interface BingAdsEventInterface {

  /**
   * Register an event.
   *
   * @param string $event
   *   The event name.
   * @param mixed $data
   *   The event data.
   */
  public function addEvent($event, $data);

  /**
   * Get the BingAds events.
   */
  public function getEvents();

}
