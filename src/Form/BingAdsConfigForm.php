<?php

namespace Drupal\bing_ads\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BingAdsConfigForm.
 *
 * @package Drupal\bing_ads\Form
 */
class BingAdsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bing_ads.bingadsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bing_ads_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bing_ads.bingadsconfig');
    $form['BingAds_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BingAds ID'),
      '#description' => $this->t('BingAds ID'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('BingAds_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('bing_ads.bingadsconfig')
      ->set('BingAds_id', $form_state->getValue('BingAds_id'))
      ->save();
  }

}
